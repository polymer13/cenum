
#[macro_export]
macro_rules! make_bitflags {

    (type: $type:ty, index: $index:expr, names: ) => {
    };

    (type: $type:ty, index: $index:expr, names:  $name:ident $(, $next_name:ident)*  ) => {
        pub const $name : Self = Self {  bits: (1 as $type) << ( $index ) } ;
        make_bitflags!(type: $type, index: $index+1, names: $($next_name),* );
    };

    (type $alias_name:ident = $type:ty; $($name:ident),+) => {

        #[derive(Debug, Clone, Copy, std::cmp::PartialEq, std::cmp::Eq)]
        pub struct $alias_name { bits: $type }

        impl $alias_name {
            pub const NOTHING : Self  = Self { bits: 0 as $type } ;
            make_bitflags!(type: $type, index: 0, names: $( $name ),+ );


            pub fn from_bits(new_bits: $type) -> Self {
                Self { bits: new_bits }
            }

            pub fn enable(&mut self, flag: Self) -> &mut Self {
                self.bits |= flag.bits;
                self
            }

             pub fn disable(&mut self, flag: Self) -> &mut Self {
                self.bits ^= flag.bits;
                self
             }

            pub fn is_enabled(&self, flag: Self) -> bool {
                if (self.bits & flag.bits) == flag.bits {
                    true
                } else {
                    false
                }
            }
        }
    };
}


#[macro_export]
macro_rules! match_bitfield {

    // (match ($lhs:expr) { $rhs:expr => { $( $text:stmt; )* } $(, $next_rhs:expr => { $( $next_text:stmt; )* } )* } ) => {
    (match ($lhs:expr) { $($rhs:expr => { $( $text:stmt; )* })* } ) => {
        $(
        if $lhs.is_enabled($rhs) { $($text;)* }
        )*
    };

}
