#[macro_export]
#[allow(unused_macros)]
macro_rules! enumerate_vals_define_const {
    (custom $type:ident, $id:ident, $value:expr) => {
        pub const $id : $type = $type ($value);
    };

    (primitive $type:ty, $id:ident, $value:expr) => {
        pub const $id : $type = $value;
    };

}

#[macro_export]
#[allow(unused_macros)]
macro_rules! enumerate_vals_with_custom_type {
    (type: $type:tt, index: $index:expr;) => {};

    (type: $type:tt, index: $index:expr; $id:ident = $value:expr$(, $($next_id:ident$(= $next_value:expr)?),*)?) => {
        enumerate_vals_define_const!(custom $type, $id, $value);
        enumerate_vals_with_custom_type!(type: $type, index: $value+1; $($($next_id $(= $next_value)?),*)?);
    };
    (type: $type:tt, index: $index:expr; $id:ident$(, $($next_id:ident$(= $next_value:expr)?),*)?) => {
        enumerate_vals_define_const!(custom $type, $id, $index);
        enumerate_vals_with_custom_type!(type: $type, index: $index+1; $($($next_id $(= $next_value)?),*)?);
    };
}

#[macro_export]
#[allow(unused_macros)]
macro_rules! enumerate_vals {
    (type: $type:ty, index: $index:expr;) => {};

    (type: $type:ty, index: $index:expr; $id:ident = $value:expr$(, $($next_id:ident$(= $next_value:expr)?),*)?) => {
        enumerate_vals_define_const!(primitive $type, $id, $value);
        enumerate_vals!(type: $type, index: $value+1; $($($next_id $(= $next_value)?),*)?);
    };

    (type: $type:ty, index: $index:expr; $id:ident$(, $($next_id:ident$(= $next_value:expr)?),*)?) => {
        enumerate_vals_define_const!(primitive $type, $id, $index);
        enumerate_vals!(type: $type, index: $index+1; $($($next_id $(= $next_value)?),*)?);
    };

    ($($id:ident$(= $value:expr)?),*) => {
        enumerate_vals!(type: u32, index: 0u32;$($id $(= $value)?),*);
    };

    (use $type:ty; $($id:ident$(= $value:expr)?),*) => {
        enumerate_vals!(type: $type, index: 0;$($id $(= $value)?),*);
    };

    (type $alias_type:ident = $value_type:ty; $($id:ident$(= $value:expr)?),+) => {

        // use std::clone::Clone;
        // use std::marker::Copy;

        #[derive(Debug, Clone, Copy, std::cmp::PartialEq, std::cmp::Eq)]
        pub struct $alias_type ($value_type);

        impl std::fmt::Display for $alias_type {
            fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                write!(f, "{}", self.0)
            }
        }

        #[allow(dead_code)]
        impl $alias_type {
            pub fn all_values() -> &'static [(&'static str, $alias_type)] {
                &[ $((stringify!($id), $id)),* ]
            }

            pub fn iter() -> std::slice::Iter<'static, (&'static str, $alias_type) > {
                Self::all_values().iter()
            }
        }


        impl $alias_type {
            #[allow(dead_code)]
            pub fn new (value: $value_type) -> $alias_type {
                $alias_type (value)
            }
        }

        enumerate_vals_with_custom_type!(type: $alias_type, index: 0; $($id $(= $value)?),*);
    };
}
