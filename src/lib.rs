#[macro_use]
pub mod enumerate;
#[macro_use]
pub mod bitflags;


#[cfg(test)]
mod tests
{
    enumerate_vals!(
        type Utensils = i32;
        FORK = 3, KNIFE = 10, SPOON = 8
    );

    enumerate_vals!(
        type FakeErrors = u64;
        FAKE_OVERFLOW, INTELLIGENCE_OVERFLOW, CORE_DUMP_LOL, YOU_PC_IS_DEAD_NOW_SORRY
    );

    enumerate_vals! (
        FILE_SYSTEM, NETWORK, MEMORY_MAP
    );

    enumerate_vals! {
        ENGINEER = 100, DOCTOR, TECHNICIAN = 200, POTATO_PEALER
    }

    enumerate_vals! {
        use u64;
        NUMBER0 = 0, NUMBER1 = 123, NUMBER2, NUMBER5
    }

    enumerate_vals! {
        use u64;
        SEQ_NUMBER0, SEQ_NUMBER1, SEQ_NUMBER2, SEQ_NUMBER5
    }

    make_bitflags! {
        type Connection = u32;
        INPUT, OUTPUT, ACCESSIBLE, ACTIVE
    }

    #[test]
    fn test_bitflags () -> std::io::Result<()> {
        assert_eq!(Connection::NOTHING, Connection::from_bits(0));
        assert_eq!(Connection::INPUT, Connection::from_bits(1));
        assert_eq!(Connection::OUTPUT, Connection::from_bits(2));
        assert_eq!(Connection::ACCESSIBLE, Connection::from_bits(4));
        assert_eq!(Connection::ACTIVE, Connection::from_bits(8));

        let mut f = Connection::ACTIVE.clone();
        f.enable(Connection::INPUT);
        assert_eq!(f, Connection::from_bits(8 | 1));

        Ok(())
    }

    #[test]
    fn test_explicit_values_custom_type () -> std::io::Result<()> {
        assert_eq!(Utensils(3), FORK);
        assert_eq!(Utensils(10), KNIFE);
        assert_eq!(Utensils(8), SPOON);
        Ok(())
    }

    #[test]
    fn test_sequential_values_custom_type () -> std::io::Result<()> {
        assert_eq!(FakeErrors( 0 ), FAKE_OVERFLOW);
        assert_eq!(FakeErrors( 1 ), INTELLIGENCE_OVERFLOW);
        assert_eq!(FakeErrors( 2 ), CORE_DUMP_LOL);
        assert_eq!(FakeErrors( 3 ), YOU_PC_IS_DEAD_NOW_SORRY);
        Ok(())
    }

    #[test]
    fn test_sequential_values_default_type () -> std::io::Result<()> {
        assert_eq!(0, FILE_SYSTEM);
        assert_eq!(1, NETWORK);
        assert_eq!(2, MEMORY_MAP);
        Ok(())
    }

    #[test]
    fn test_coexisting_explicit_and_sequential() -> std::io::Result<()> {
        assert_eq!(100, ENGINEER);
        assert_eq!(101, DOCTOR);
        assert_eq!(200, TECHNICIAN);
        assert_eq!(201, POTATO_PEALER);
        Ok(())
    }

    #[test]
    fn test_use_primitive_type_explicit() -> std::io::Result<()> {
        assert_eq!(0u64, NUMBER0);
        assert_eq!(123u64, NUMBER1);
        assert_eq!(124u64, NUMBER2);
        assert_eq!(125u64, NUMBER5);
        Ok(())
    }

    #[test]
    fn test_use_primitive_type_sequential() -> std::io::Result<()> {
        assert_eq!(0, SEQ_NUMBER0);
        assert_eq!(1, SEQ_NUMBER1);
        assert_eq!(2, SEQ_NUMBER2);
        assert_eq!(3, SEQ_NUMBER5);
        Ok(())
    }

    #[test]
    fn test_runtime_value_ids() -> std::io::Result<()> {

        let _e = FakeErrors::all_values();

        for (name, value) in _e.iter() {
            println!("{} = {}", name, value);
        }

        let _i = FakeErrors::iter();

        for (name, value) in _i {
            println!("{} = {}", name, value);
        }

        Ok(())
    }
}
